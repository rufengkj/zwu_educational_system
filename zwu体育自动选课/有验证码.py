#!/opt/python3/bin/python3
#-*- coding utf-8 -*-
# coder DIRICHLET
# python3.6

import requests
import re
import urllib.parse
from pyquery import PyQuery as pq
from PIL import Image


class zwujw:
    def __init__(self):
        self.s=requests.session()
        self.s = requests.session()

    # 获得self.xh(学号),self.key(密码),self.name(姓名)
    def dl(self, xh, key):  # 获得self.xh(学号),self.key(密码),self.name(姓名)
        self.xh = xh
        self.key = key
        url = 'http://jwxt.zwu.edu.cn/'
        sendurl = 'http://jwxt.zwu.edu.cn/Default2.aspx'
        reqt = self.s.get(url)
        __VIEWSTATE = re.findall("name=\"__VIEWSTATE\" value=\"(.*?)\"", reqt.text)[0]
        __VIEWSTATEGENERATOR = re.findall("name=\"__VIEWSTATEGENERATOR\" value=\"(.*?)\"", reqt.text)[0]
        req = self.s.get('http://jwxt.zwu.edu.cn/CheckCode.aspx?')
        with open('picture.gif', 'wb') as file:
            file.write(req.content)
        img = Image.open(r'picture.gif')
        img.show()
        yzm = input('请输入验证码')
        print('您输入了', yzm)
        header = {
            'Referer': 'http://jwxt.zwu.edu.cn/',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0',
        }
        data = {
            '__VIEWSTATE': __VIEWSTATE,
            '__VIEWSTATEGENERATOR': __VIEWSTATEGENERATOR,
            'TextBox1': self.xh,
            'TextBox2': self.key,
            'TextBox3': yzm,
            'RadioButtonList1': '%D1%A7%C9%FA',
            "Button1": "",
        }
        req1 = self.s.post(sendurl, data=data, headers=header)
        p = pq(req1.text)
        t=p('#xhxm')
        if (len(t)):
            print('登陆成功')
            print(t.text())
            self.name=t.text()[11:-2]
            return 1
        else:
            print('登陆失败')
            t=p('script').eq(0)
            if(len(t.text())>25):
                print('账号密码错误')
            else:
                print(t.text()[7:-3])
            return 0


#获得self.url(发送网址),self.VIEWSTATE(状态代码),self.xmd(大项目代码与项目名称的字典)
    def tyxk_find(self):#查询是否选课
        print(self.name)
        xm = urllib.parse.quote(self.name.encode('gb2312'))
        self.url = 'http://jwxt.zwu.edu.cn/xstyk.aspx?xh=' + self.xh + '&xm=' + xm + '&gnmkdm=N121102'
        html = self.s.get(self.url)
        p = pq(html.text)
        listbox3 = p('#ListBox3').find('option')
        if (len(listbox3) == 0):
            print('您尚未选课，可以继续')
            res = 'name="__VIEWSTATE" value="(.*?)"'
            # print(html.text)
            req = re.findall(res, html.text)[0]
            self.VIEWSTATE = urllib.parse.quote(req.encode('gb2312')).replace('/n', '%2Fn')
            res = '<option value="(.*?)">(.*?)</option>'
            xm = re.findall(res, html.text)[2:]
            for i in xm:
                print(i[0] + '  ' + i[1])
            self.xmd = dict(xm)
            return 1
        else:
            print('您已选课 ' + listbox3.text())
            return 0

#获得self.VIEWSTATE_TWO(第二个状态代码),list2
    def tyxk_get_list2(self,xm):#
        try:
            self.xm=xm
            print('您选择的是 '+xm+' '+self.xmd[xm])

        except:
            print('项目代码错误')
            print('')
            return 0
        else:

            dataone='__EVENTTARGET=ListBox1&__EVENTARGUMENT=&__VIEWSTATE='+self.VIEWSTATE+'&__VIEWSTATEGENERATOR=FA639717&DropDownList1=%CF%EE%C4%BF&ListBox1='+xm
            self.header = {
                'Referer': 'http://jwxt.zwu.edu.cn/xstyk.aspx?xh=2017010609&xm=%BD%F0%C9%FD%EC%C7&gnmkdm=N121102',
                'Host': 'jwxt.zwu.edu.cn',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/58.0',
                'Content-Type':'application/x-www-form-urlencoded',


            }

            html = self.s.post(self.url, data=dataone, headers=self.header)
            html.encoding = 'gb2312'
            res = 'name="__VIEWSTATE" value="(.*?)"'
            req = re.findall(res, html.text)[0]
            self.VIEWSTATE_TWO = urllib.parse.quote(req.encode('gb2312')).replace('/n','%2Fn')
            p=pq(html.text)
            j=1
            self.di = {}
            for i in p('#ListBox2 option').items():
                print(j,i.text())
                self.di[j]=i.attr('value')
                j=j+1



            return 1



    def tyxk_send(self,xmdm):
        data='__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE='+self.VIEWSTATE_TWO+'&__VIEWSTATEGENERATOR=FA639717&DropDownList1=%CF%EE%C4%BF&ListBox1='+self.xm+'&ListBox2='+self.di[int(xmdm)]+'&RadioButtonList1=0&button3=%d1%a1%b6%a8%bf%ce%b3%cc'
        html=self.s.post(self.url,data=data,headers=self.header)
        p = pq(html.text)
        listbox3 = p('#ListBox3').find('option')
        if(len(listbox3)==0):
            print('')
            print('选课失败')
            js=p('script').eq(0).text()[7:-3]
            print(js)
            return 0
        else:
            print('选课成功')
            return 1


def main():
    zwu = zwujw()
    if(zwu.dl(input('请输入学号'),input('请输入密码'))):
        if (zwu.tyxk_find() == 1):
            while (True):
                if (zwu.tyxk_get_list2(input('请选择一个项目')) == 1):
                    break
            xmdm = input('请选择一个具体项目(输入1-30的数字代码)')
            while (True):
                if(zwu.tyxk_send(xmdm)):
                    break
        input('按任意键退出')



if __name__=='__main__':
    main()

