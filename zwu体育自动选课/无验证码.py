#!/opt/python3/bin/python3
#-*- coding utf-8 -*-
# coder DIRICHLET
# python3.6
import requests
import re
import urllib.parse
from pyquery import PyQuery as pq


class zwujw:
    def __init__(self):
        self.s=requests.session()
        self.s = requests.session()
        self.xh = ''
        self.name = ''
        self.VIEWSTATE=''

# 获得self.xh(学号),self.key(密码),self.name(姓名)
    def dl(self,xh,key):#获得self.xh(学号),self.key(密码),self.name(姓名)
        self.xh=xh
        self.key = key
        html=self.s.get('http://portal.zwu.edu.cn:8080/cas/login?service=http://portal.zwu.edu.cn/neusoftcas.jsp').text
        #print(html)
        res='name=\"lt\" value=\"(.*?)\"'
        lt= re.findall(res,html)
        data={
            'encodedService':'http%3a%2f%2fportal.zwu.edu.cn%2fneusoftcas.jsp',
            'service':'http: // portal.zwu.edu.cn / neusoftcas.jsp',
            'serviceName':'null',
            'username':self.xh,
            'password':self.key,
            'lt':lt,
            'userNameType':'cardID'
        }
        res='<title>(.*?)</title>|<TITLE>(.*?)</TITLE>'
        html=self.s.post('http://portal.zwu.edu.cn:8080/cas/login',data=data)
        if(re.findall(res,html.text)[0][0]=='CAS认证转向'):
            #print(html.text)
            '''htmls=s.get('http://portal.zwu.edu.cn:8080/cas/login?service=http://portal.zwu.edu.cn/neusoftcas.jsp').text
            #print(htmls)
            res='href=\"(.*?)\"'
            y=re.findall(res,htmls)[0]
            print(s.get(y).text)'''
            htmls=self.s.get("http://portal.zwu.edu.cn/eapdomain/neudcp/sso/sso_jw.jsp?server=jwxt.zwu.edu.cn").text
            res='href=\"(.*?)\"'
            url=re.findall(res,htmls)[0]
            con = self.s.get(url).text
            res = r'欢迎您：<em><span id="xhxm">(.*?)</span></em>'
            try:
                message = re.findall(res,con)[0]
            except:
                print('登陆错误')
                return 0
            else:
                print('登陆成功')
                print('欢迎你：'+message)
                self.name=message[-5:-2]
                return 1
        elif(re.findall(res,html.text)[0][1]=='欢迎登录'):#密码错误
            res='<div style="color: #FF0000">(.*?)</div>'
            print(re.findall(res,html.text)[0])
            return 0
        else:
            print('未知错误，请联系管理员')
            return 0


#获得self.url(发送网址),self.VIEWSTATE(状态代码),self.xmd(大项目代码与项目名称的字典)
    def tyxk_find(self):#查询是否选课
        xm = urllib.parse.quote(self.name.encode('gb2312'))
        self.url = 'http://jwxt.zwu.edu.cn/xstyk.aspx?xh=' + self.xh + '&xm=' + xm + '&gnmkdm=N121102'
        html = self.s.get(self.url)
        p = pq(html.text)
        listbox3 = p('#ListBox3').find('option')

        if (len(listbox3) == 0):
            print('您尚未选课，可以继续')
            res = 'name="__VIEWSTATE" value="(.*?)"'
            # print(html.text)
            req = re.findall(res, html.text)[0]
            self.VIEWSTATE = urllib.parse.quote(req.encode('gb2312')).replace('/n', '%2Fn')
            res = '<option value="(.*?)">(.*?)</option>'
            xm = re.findall(res, html.text)[2:]
            for i in xm:
                print(i[0] + '  ' + i[1])
            self.xmd = dict(xm)
            return 1
        else:
            print('您已选课 ' + listbox3.text())
            return 0

#获得self.VIEWSTATE_TWO(第二个状态代码),list2
    def tyxk_get_list2(self,xm):#
        try:
            self.xm=xm
            print('您选择的是 '+xm+' '+self.xmd[xm])
            '''data={
                '__VIEWSTATE':self.VIEWSTATE,
                'DropDownList1':'%CF%EE%C4%BF',
                'ListBox1':xm,
                '__VIEWSTATEGENERATOR':'FA639717',
                '__EVENTTARGET':'ListBox1',
                '__EVENTARGUMENT':'',
            }'''

        except:
            print('项目代码错误')
            print('')
            return 0
        else:

            dataone='__EVENTTARGET=ListBox1&__EVENTARGUMENT=&__VIEWSTATE='+self.VIEWSTATE+'&__VIEWSTATEGENERATOR=FA639717&DropDownList1=%CF%EE%C4%BF&ListBox1='+xm
            self.header = {
                'Referer': 'http://jwxt.zwu.edu.cn/xstyk.aspx?xh=2017010609&xm=%BD%F0%C9%FD%EC%C7&gnmkdm=N121102',
                'Host': 'jwxt.zwu.edu.cn',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/58.0',
                'Content-Type':'application/x-www-form-urlencoded',


            }

            html = self.s.post(self.url, data=dataone, headers=self.header)
            html.encoding = 'gb2312'
            res = 'name="__VIEWSTATE" value="(.*?)"'
            req = re.findall(res, html.text)[0]
            self.VIEWSTATE_TWO = urllib.parse.quote(req.encode('gb2312')).replace('/n','%2Fn')
            #print(html.text)
            p=pq(html.text)
            j=1
            self.di = {}
            for i in p('#ListBox2 option').items():
                print(j,i.text())
                self.di[j]=i.attr('value')
                j=j+1



            return 1



    def tyxk_send(self,xmdm):
        data='__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE='+self.VIEWSTATE_TWO+'&__VIEWSTATEGENERATOR=FA639717&DropDownList1=%CF%EE%C4%BF&ListBox1='+self.xm+'&ListBox2='+self.di[int(xmdm)]+'&RadioButtonList1=0&button3=%d1%a1%b6%a8%bf%ce%b3%cc'
        html=self.s.post(self.url,data=data,headers=self.header)
        p = pq(html.text)
        listbox3 = p('#ListBox3').find('option')

        if(len(listbox3)==0):
            print('')
            print('选课失败')
            js=p('script').eq(0).text()[7:-3]
            print(js)
            return 0
        else:
            print('选课成功')
            return 1


def main():
    zwu = zwujw()
    if(zwu.dl(input('请输入学号'),input('请输入密码'))):
        if (zwu.tyxk_find() == 1):
            while (True):
                if (zwu.tyxk_get_list2(input('请选择一个项目')) == 1):
                    break
            xmdm = input('请选择一个具体项目(输入1-30的数字代码)')
            while (True):
                if(zwu.tyxk_send(xmdm)):
                    break


        input('按任意键退出')



if __name__=='__main__':
    main()
